const redis = require("redis");
const logger = require("./logger");

const redisClient = redis.createClient({
  url: process.env.REDIS_URL,
});

redisClient.connect();

const package = require("../package.json");

module.exports = redisClient;

redisClient.HSET("versions", package.name, package.version);

setInterval(() => {
  heartBeat();
}, 60000);

heartBeat();

function heartBeat() {
  redisClient.HSET("heartbeats", package.name, Date.now());
}
