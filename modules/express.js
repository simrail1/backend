const express = require("express");
const logger = require("./logger");
const app = express();
const fs = require("fs");
const pinoHttp = require("pino-http");
app.use(express.json());

// add pino middleware

if (process.env.HTTP_LOGGING) {
  app.use(
    pinoHttp({
      logger: logger,
    })
  );
}

fs.readdirSync("./routes").forEach((file) => {
  const route = require("../routes/" + file);
  app.use("/api/" + file.split(".")[0], route);
  logger.info(`Loaded route /api/${file.split(".")[0]}`);
});

// start

const port = process.env.PORT || 3000;

app.listen(port, () => {
  logger.info("Listening on port " + port);
});
