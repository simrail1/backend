const router = require("express").Router();
const redis = require("../modules/redis");

router.get("/:server", async (req, res) => {
  const server = req.params.server;
  let trains = await redis.HGET("trains", server);
  if (!trains) {
    redis.HDEL("trains", server);
    return res.sendStatus(404);
  }
  trains = JSON.parse(trains);

  const trainsWithDriver = await Promise.all(
    trains.map(async (train) => {
      if (train.driver) {
        train.driver = await redis.HGET("players", train.driver);
        train.driver = JSON.parse(train.driver);
      }
      return train;
    })
  );

  res.send(trainsWithDriver);
});

module.exports = router;
