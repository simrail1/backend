const router = require("express").Router();
const redis = require("../modules/redis");

router.get("/", async (req, res) => {
  const servers = await redis.HGETALL("servers");

  res.send(
    convertObjectToArray(servers).sort((a, b) => a.name.localeCompare(b.name))
  );
});

module.exports = router;

function convertObjectToArray(obj) {
  const arr = [];
  // Loop through each key-value pair in the object
  for (const key in obj) {
    // Parse the JSON string value into a JavaScript object
    const valueObject = JSON.parse(obj[key]);
    // Push the parsed object to the array
    arr.push(valueObject);
  }
  return arr;
}
