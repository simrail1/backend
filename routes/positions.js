const router = require("express").Router();
const redis = require("../modules/redis");
const logger = require("../modules/logger");

router.get("/train/:trainId", async (req, res) => {
  const trainId = req.params.trainId;
  let positions = await redis.HGET("trains-positions", trainId);
  if (!positions) {
    return res.sendStatus(404);
  }
  positions = JSON.parse(positions);
  res.send(positions.positions);
});

module.exports = router;
