const router = require("express").Router();
const redis = require("../modules/redis");

router.get("/:server", async (req, res) => {
  const server = req.params.server;
  let stations = await redis.HGET("stations", server);

  stations = JSON.parse(stations);

  stations = await Promise.all(
    stations.map(async (station) => {
      if (station.dispatcher) {
        station.dispatcher = JSON.parse(
          await redis.HGET("players", station.dispatcher)
        );
      }

      return station;
    })
  );

  res.send(stations);
});

module.exports = router;
