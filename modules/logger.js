const pino = require("pino");
const { LOG_LEVEL, LOKI_URL, PWD } = process.env;
const { name, version } = require("../package.json");
const { LokiOptions } = require("pino-loki");

const transport = pino.transport({
  targets: [
    ...(LOKI_URL
      ? [
          {
            target: "pino-loki",
            options: {
              batching: true,
              labels: {
                job: "NodeJS",
                project: "Simrail",
                name,
                version,
              },
              interval: 5,
              host: LOKI_URL,
            },
          },
        ]
      : []),
    { target: "pino-pretty" },
  ],
});

const logger = pino(
  {
    name,
    level: LOG_LEVEL || "info",
    version,
  },
  transport
);

module.exports = logger;
