const router = require("express").Router();
const redis = require("../modules/redis");
const logger = require("../modules/logger");

router.get("/train/:trainNumber", async (req, res) => {
  const trainNumber = req.params.trainNumber;
  try {
    let timeTable = await redis.HGET("timetables:train", trainNumber);
    timeTable = JSON.parse(timeTable);
    logger.info(`Serving timeTable for train ${trainNumber}`);
    res.send(timeTable.timeTable);
  } catch (error) {
    logger.error(error);
    res.sendStatus(500);
  }
});

module.exports = router;
